# Copyright © 2020 Olaf Meeuwissen <paddy-hack@member.fsf.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class Record < ApplicationRecord
  has_many :entries, dependent: :destroy
  accepts_nested_attributes_for :entries

  validates_presence_of :date
  validates_presence_of :description
end
