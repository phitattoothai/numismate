# Copyright © 2020 Olaf Meeuwissen <paddy-hack@member.fsf.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class Entry < ApplicationRecord
  belongs_to :book
  belongs_to :record

  def self.ordered(order)
    order = :desc if order.blank?
    joins(:record).
      order("records.date": order)
  end

  monetize :amount_subunits
end
