# Copyright © 2020 Olaf Meeuwissen <paddy-hack@member.fsf.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class Book < ApplicationRecord
  has_ancestry
  has_many :entries

  validates_presence_of :name

  def balance
    entries.sum(:amount_subunits).to_money.format
  end

  def name_for_select
    "#{'−' * depth} #{name}"
  end

  def path_name
    path.pluck(:name).join('/')
  end

  def self.arrange_as_array(options={}, hash=nil)
    hash ||= arrange(options)

    arr = []
    hash.each do |node, children|
      arr << node
      arr += arrange_as_array(options, children) unless children.empty?
    end
    arr
  end
end
