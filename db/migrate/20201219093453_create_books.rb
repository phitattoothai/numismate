# Copyright © 2020 Olaf Meeuwissen <paddy-hack@member.fsf.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class CreateBooks < ActiveRecord::Migration[6.0]
  def change
    create_table :books do |t|
      t.string :name, null: false
      t.text :notes

      t.timestamps
    end
  end
end
