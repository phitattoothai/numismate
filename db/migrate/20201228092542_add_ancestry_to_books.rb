class AddAncestryToBooks < ActiveRecord::Migration[6.0]
  def change
    add_column :books, :ancestry, :string
    add_index :books, :ancestry
  end
end
