# Copyright © 2020 Olaf Meeuwissen <paddy-hack@member.fsf.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

MoneyRails.configure do |config|
  # Register a locale-independent custom currency and make it the
  # application-wide default.
  config.register_currency = {
    priority:            999,
    iso_code:            "XXX",
    iso_numeric:         "999",
    name:                "No currency",
    symbol:              "¤",
    subunit_to_unit:     1
  }
  config.default_currency = :xxx

  # Let the currency itself determine formatting conventions rather
  # than the current locale (:i18n) or none at all (nil) but always
  # show the subunits (cents) even if there are none.
  config.locale_backend = :currency
  config.no_cents_if_whole = false

  # Round monetary amounts to the nearest subunit.
  config.rounding_mode = BigDecimal::ROUND_HALF_UP

  # Set ActiveRecord column defaults for monetize'd objects.
  config.amount_column = { postfix: '_subunits',
                           type: :integer,
                           present: true,
                           null: false,
                           default: 0
                         }
  config.currency_column = { postfix: '_currency',
                             type: :string,
                             present: true,
                             null: false,
                             default: :xxx
                           }

  # Enable sanity checking for monetized fields.
  config.include_validations = true
  config.raise_error_on_money_parsing = true
end
