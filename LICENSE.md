---
Copyright: © 2020 Olaf Meeuwissen <paddy-hack@member.fsf.org>
SPDX-License-Identifier: CC-BY-SA-4.0
---

# License Policy

The `numismate` project is [Free Software][].

It gives you the Freedom to run (for any purpose), study, copy, modify
and share the project's sources, as is or modified.  However, it also
wants to make sure that the people you share with receive the very
same Freedoms you enjoyed.  To that end, all of the project's source
*code* is licensed under the terms of the [GNU Affero General Public
License][AGPL], version 3 or, at your option, any later version.

Not everything is *code*, so for documentation and artwork use of the
[Creative Commons Attribution-ShareAlike 4.0 International][CC-BY-SA],
license may be used as well.  Matter of fact, this file is licensed
that way.

Finally, some files are so "trivial" that a license may seem overkill.
However, unless a license is explicitly assigned, there is nothing
that will tell you what you can and cannot do with such a file.  The
[Creative Commons Public Domain Dedication][CC0] license can be used
for those kind of files.

 [Free Software]: https://en.wikipedia.org/wiki/Free_software
 [AGPL]: LICENSES/AGPL-3.0-or-later.txt
 [CC-BY-SA]: LICENSES/CC-BY-SA-4.0.txt
 [CC0]: LICENSES/CC0-1.0.txt

## License Compatibility

The `numismate` project depends on other software, lots of it and in
particular [Ruby on Rails][rails] and [Ruby Gems][gems].  In order to
keep `numismate` [Free Software][], it needs to avoid dependencies on
software that does not give you the Freedoms that `numismate` intends
to give you.  To achieve this, all of `numismate`'s dependencies need
to be compatible with the licenses used by `numismate` itself.

Compatible licenses have no mutually contradictory requirements.  The
`numismate` project keeps an incomplete [list of allowed and denied
licenses][policies] for software dependencies.  Dependencies with
licenses that are not (yet) allowed are flagged during CI/CD.  Such
licenses will need to be approved manually before code can be merged
to the default branch.

 [gems]: https://rubygems.org/
 [rails]: https://rubyonrails.org/
 [policies]: https://gitlab.com/numismate/numismate/-/licenses#policies
