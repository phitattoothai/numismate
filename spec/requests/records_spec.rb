# Copyright © 2020 Olaf Meeuwissen <paddy-hack@member.fsf.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'rails_helper'

RSpec.describe "/records", type: :request do
  let(:valid_attributes) {
    { date: Date.today,
      description: "Groceries"
    }
  }

  let(:invalid_attributes) {
    { date: Date.today,
      description: ''
    }
  }

  describe "GET /index" do
    it "renders a successful response" do
      Record.create! valid_attributes
      get records_url
      expect(response).to be_successful
    end
  end

  describe "GET /show" do
    it "renders a successful response" do
      record = Record.create! valid_attributes
      get record_url(record)
      expect(response).to be_successful
    end
  end

  describe "GET /new" do
    it "renders a successful response" do
      get new_record_url
      expect(response).to be_successful
    end
  end

  describe "GET /edit" do
    it "render a successful response" do
      record = Record.create! valid_attributes
      get edit_record_url(record)
      expect(response).to be_successful
    end
  end

  describe "POST /create" do
    context "with valid parameters" do
      it "creates a new Record" do
        expect {
          post records_url, params: { record: valid_attributes }
        }.to change(Record, :count).by(1)
      end

      it "redirects to the created record" do
        post records_url, params: { record: valid_attributes }
        expect(response).to redirect_to(record_url(Record.last))
      end
    end

    context "with invalid parameters" do
      it "does not create a new Record" do
        expect {
          post records_url, params: { record: invalid_attributes }
        }.to change(Record, :count).by(0)
      end

      it "renders a successful response (i.e. to display the 'new' template)" do
        post records_url, params: { record: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "PATCH /update" do
    context "with valid parameters" do
      let(:new_attributes) {
        { date: Date.yesterday,
          description: 'Rent'
        }
      }

      it "updates the requested record" do
        record = Record.create! valid_attributes
        patch record_url(record), params: { record: new_attributes }
        record.reload
        expect(record.description).to eq('Rent')
      end

      it "redirects to the record" do
        record = Record.create! valid_attributes
        patch record_url(record), params: { record: new_attributes }
        record.reload
        expect(response).to redirect_to(record_url(record))
      end
    end

    context "with invalid parameters" do
      it "renders a successful response (i.e. to display the 'edit' template)" do
        record = Record.create! valid_attributes
        patch record_url(record), params: { record: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "DELETE /destroy" do
    it "destroys the requested record" do
      record = Record.create! valid_attributes
      expect {
        delete record_url(record)
      }.to change(Record, :count).by(-1)
    end

    it "redirects to the records list" do
      record = Record.create! valid_attributes
      delete record_url(record)
      expect(response).to redirect_to(records_url)
    end
  end
end
