# Copyright © 2020 Olaf Meeuwissen <paddy-hack@member.fsf.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'rails_helper'

RSpec.describe Book, type: :model do
  let(:valid_attributes) {
    { name: 'Expenses' }
  }

  it "is valid with valid attributes" do
    expect(Book.new valid_attributes).to be_valid
  end

  it "is not valid without a name" do
    book = Book.new(name: nil)
    expect(book).to_not be_valid
  end

  it "is not valid with an empty name" do
    book = Book.new(name: '')
    expect(book).to_not be_valid
  end

  it "is not valid with a blank name" do
    book = Book.new(name: "\t")
    expect(book).to_not be_valid
  end
end
