# Copyright © 2020 Olaf Meeuwissen <paddy-hack@member.fsf.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'rails_helper'

RSpec.describe Entry, type: :model do
  before(:context) do
    @book = Book.create! name: "Book"
    @record = Record.create! date: Date.today, description: "Record"
  end

  after(:context) do
    @book.destroy!
    @record.destroy!
  end

  let(:valid_attributes) {
    { amount: "10 JPY".to_money,
      book: @book,
      record: @record
    }
  }

  it "is valid with valid attributes" do
    expect(Entry.new valid_attributes).to be_valid
  end

  it "is have a zero amount by default" do
    entry = Entry.new book: @book, record: @record
    expect(entry).to be_valid
    expect(entry.amount).to eq(0)
  end

  it "is not valid without a record reference" do
    entry = Entry.new amount: "10 JPY".to_money, book: @book
    expect(entry).to_not be_valid
  end

  it "is not valid without a book reference" do
    entry = Entry.new amount: "10 JPY".to_money, record: @record
    expect(entry).to_not be_valid
  end
end
