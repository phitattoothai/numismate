# Copyright © 2020 Olaf Meeuwissen <paddy-hack@member.fsf.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'rails_helper'

RSpec.describe Record, type: :model do
  let(:valid_attributes) {
    { date: Date.today,
      description: 'Groceries'
    }
  }

  it "is valid with valid attributes" do
    expect(Record.new valid_attributes).to be_valid
  end

  it "is not valid without a date" do
    record = Record.new(date: '')
    expect(record).to_not be_valid
  end

  it "is not valid without a description" do
    record = Record.new(description: '')
    expect(record).to_not be_valid
  end
end
