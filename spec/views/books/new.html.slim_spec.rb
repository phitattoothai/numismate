# Copyright © 2020 Olaf Meeuwissen <paddy-hack@member.fsf.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'rails_helper'

RSpec.describe "books/new", type: :view do
  before(:each) do
    assign(:book, Book.new(
      name: "MyString",
      notes: "MyText"
    ))
  end

  it "renders new book form" do
    render
    assert_select "form[action=?][method=?]", books_path, "post" do
      assert_select "input[name=?]", "book[name]"
      assert_select "textarea[name=?]", "book[notes]"
    end
  end
end
