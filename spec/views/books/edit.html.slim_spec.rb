# Copyright © 2020 Olaf Meeuwissen <paddy-hack@member.fsf.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'rails_helper'

RSpec.describe "books/edit", type: :view do
  before(:each) do
    @book = assign(:book, Book.create!(
      name: "MyString",
      notes: "MyText"
    ))
  end

  it "renders the edit book form" do
    render
    assert_select "form[action=?][method=?]", book_path(@book), "post" do
      assert_select "input[name=?]", "book[name]"
      assert_select "textarea[name=?]", "book[notes]"
    end
  end
end
