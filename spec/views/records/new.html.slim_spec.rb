# Copyright © 2020 Olaf Meeuwissen <paddy-hack@member.fsf.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'rails_helper'

RSpec.describe "records/new", type: :view do
  before(:each) do
    assign(:record, Record.new(
      description: "MyString"
    ))
  end

  it "renders new record form" do
    render
    assert_select "form[action=?][method=?]", records_path, "post" do
      assert_select "input[name=?]", "record[description]"
    end
  end
end
