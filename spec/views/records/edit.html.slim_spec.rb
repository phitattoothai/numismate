# Copyright © 2020 Olaf Meeuwissen <paddy-hack@member.fsf.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'rails_helper'

RSpec.describe "records/edit", type: :view do
  before(:each) do
    @record = assign(:record, Record.create!(
      date: Date.today,
      description: "MyString"
    ))
  end

  it "renders the edit record form" do
    render
    assert_select "form[action=?][method=?]", record_path(@record), "post" do
      assert_select "input[name=?]", "record[description]"
    end
  end
end
