# Copyright © 2020 Olaf Meeuwissen <paddy-hack@member.fsf.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'rails_helper'

RSpec.describe "records/index", type: :view do
  before(:each) do
    assign(:records, [
      Record.create!(
        date: Date.today,
        description: "Description"
      ),
      Record.create!(
        date: Date.yesterday,
        description: "Description"
      )
    ])
  end

  it "renders a list of records" do
    render
    assert_select "tr>td", text: "Description".to_s, count: 2
  end
end
