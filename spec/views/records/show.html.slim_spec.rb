# Copyright © 2020 Olaf Meeuwissen <paddy-hack@member.fsf.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'rails_helper'

RSpec.describe "records/show", type: :view do
  before(:each) do
    @record = assign(:record, Record.create!(
      date: Date.today,
      description: "Description"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Description/)
  end
end
